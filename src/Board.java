
public class Board {
	char table[][] = {
			{'-','-','-'},
			{'-','-','-'},
			{'-','-','-'}
	};

	Player x;
	Player o;
	Player winner;
	Player current;
	int turnCount;
	
	public Board(Player x, Player o) {
		this.x = x;
		this.o = o;
		current = null;
		turnCount = 0;
	}
	
	public char[][] getTable(){
		return table;
	}
	
	public Player getCurrent() {
		return current;
	}
	
	public boolean setTable(int row, int cow) {
		
		return false;
	}
	
	public void switchTurn() {
		if(current == x) {
			current = o;
		}else {
			current = x;
		}
		turnCount++;

	}
	
	public boolean isFinish() {
		return true;
	}
}
