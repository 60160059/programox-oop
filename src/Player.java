
public class Player {
	char name;
	int win;
	int lose;
	int draw;
	
	public Player(char name) {
		this.name = name;
	}

	public char getName() {
		return name;
	}
	
	public void win() {
		win++;
	}
	
	public void lose() {
		lose++;
	}
	
	public void draw() {
		draw++;
	}
	
	public int getWin() {
		return win;	
	}
	
	public int getLose() {
		return lose;
	}
	
	public int getDraw() {
		return draw;
		
	}
	
	
}
